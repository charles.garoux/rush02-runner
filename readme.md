# Runner Rush02

## Presentation

### Context
A l'école [42 Lyon](https://42lyon.fr/), **les tuteurs du pôle “pédagogie”**, entre autres missions, organisent des “Rush”.

**Un “Rush” est un travail en petit groupe d'étudiants** avec une forte contrainte de temps (souvent de 2 à 3 jours selon le Rush).

**Le Rush du projet** :
Ce Rush est le "Rush02", **il a pour sujet de développer une IA** pour faire un duel proche du jeu “puissance 4”.
	
Le duel est supervisé par un programme d’arbitrage, fourni par les tuteurs.

**Dépôt du projet** (sujet, arbitre, bots et documentation pour les étudiants) : https://gitlab.com/guhernan/rush02-player-open

### La CLI

**Presentation general** :
Ce projet est **une partie de l'environnement développé** par les tuteurs de [42 Lyon](https://42lyon.fr/) pour le Rush02.

Cette partie est une **CLI** qui **permet d’automatiser** certains aspects du Rush02 :
* la vérification l'accès aux IA des joueurs (dépôt Git et constructibilité de l’image Docker)
* récupération des IA des joueurs (récupération des tous les dépôts)
* lancement des duel d’IA (construction des images Docker, lancement du duel avec l’arbitre et enregistrement du résultat du duel)

**Presentation technique** : [/runner-cli](/runner-cli/readme.md)

**Documentation technique** : [/runner-cli/doc](/runner-cli/doc/readme.md)

**Démonstration** : [/runner-cli/demo](/runner-cli/demo/readme.md)