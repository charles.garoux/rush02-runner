import Referee from '../lib/referee';
import Logger from '../../common/lib/logger';
import {FileSystemPath} from '../../common/data-structures/commons.types';

export default async function runDuelServer(playerAProgram: FileSystemPath, playerBProgram: FileSystemPath) {
    Logger.duelCli.info('Start run referee');
    const duelResultJson: string = await Referee.run(playerAProgram, playerBProgram);
    Logger.duelCli.info('End run referee');
    return duelResultJson;
}
