import getPlayers from './get-players';
import buildPlayerProgram from './build-player-program';
import runDuelServer from './run-duel-server';
import installDuelServer from './install-duel-server';
import generateContainerWrappingScripts from './generate-container-wrapping-scripts';
import cleanUpDuelPlayers from './clean-up-duel';
import registerDuelResult from './register-duel-result';

export default {
    getPlayers,
    buildPlayerProgram,
    installDuelServer,
    runDuelServer,
    generateContainerWrappingScripts,
    registerDuelResult,
    cleanUpDuelPlayers
};
