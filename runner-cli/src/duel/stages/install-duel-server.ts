import Referee from '../lib/referee';
import Logger from '../../common/lib/logger';
import {FileSystemPath} from '../../common/data-structures/commons.types';

export default async function installDuelServer(pathToReferee: FileSystemPath) {
    Logger.duelCli.info('Start install referee');
    await Referee.init(pathToReferee);
    Logger.duelCli.info('Finish install referee');
}
