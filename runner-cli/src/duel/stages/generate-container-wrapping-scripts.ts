import Logger from '../../common/lib/logger';
import * as Dockerode from 'dockerode';
import * as fs from 'fs';
import {promises as fsPromises} from 'fs';
import Player from '../data-structures/player';
import Paths from '../config/paths';
import {createAllDir, resetAllDir} from '../../common/lib/utils';
import * as path from 'path';

type PlayerWithDockerInfo = Player & { dockerImageInfo: Dockerode.ImageInspectInfo }
export type ContainerWrappingScriptData = { path: string, containerName: string }

function formatContainerName(playerId: string) {
    return `player-${playerId}`;
}

function generateScriptContent(dockerImage: string, containerName?: string): string {
    const limitFlags = '--cpus="1" --memory="128m" --kernel-memory="192m" --memory-swap="192m" --network none';
    return `#!/usr/bin/env bash
docker run ${limitFlags} -i ${containerName ? `--name ${containerName}` : ''} ${dockerImage}`;
}

async function generateWrappingScriptFile(player: PlayerWithDockerInfo): Promise<ContainerWrappingScriptData> {
    const pathToScript = path.resolve(Paths.toWrappingScript(player.id));
    const scriptChmod = '700';
    const containerName = formatContainerName(player.id);

    createAllDir([path.dirname(pathToScript)]);

    if (fs.existsSync(pathToScript))
        await fsPromises.unlink(pathToScript);

    await fsPromises.writeFile(pathToScript, generateScriptContent(player.dockerImageInfo.RepoTags[0], containerName));
    await fsPromises.chmod(pathToScript, scriptChmod);

    Logger.duelCli.verbose(`Player "${player.id}" wrapping script generate to "${pathToScript}" with chmod "${scriptChmod}"`);

    return {path: pathToScript, containerName};
}

export default async function generateContainerWrappingScripts(playerA: PlayerWithDockerInfo, playerB: PlayerWithDockerInfo) {
    Logger.duelCli.info(`Start generate wrapping scripts stage`);

    const playersImageInfos: PlayerWithDockerInfo[] = [playerA, playerB];

    const pathsToWrappingScriptsPromises: Promise<ContainerWrappingScriptData>[] = playersImageInfos.map(async (player): Promise<ContainerWrappingScriptData> => {
        try {
            return await generateWrappingScriptFile(player);
        } catch (error) {
            Logger.duelCli.error(error, {
                errorDescription: `Generate player "${player.id}" wrapping script failed`,
                suspectedFunction: generateWrappingScriptFile
            });
            process.exit(40);
        }
    });

    const [playerAScript, playerBScript] = await Promise.all(pathsToWrappingScriptsPromises);

    Logger.duelCli.info(`End generate wrapping scripts stage`);
    return {
        playerAScript,
        playerBScript
    };
}
