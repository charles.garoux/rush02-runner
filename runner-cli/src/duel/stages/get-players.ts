import Player from '../data-structures/player';
import Logger from '../../common/lib/logger';

type StageResult = Promise<{ playerA: Player, playerB: Player }>

export default async function getPlayers(playerAId: string, playerBId: string): StageResult {
    Logger.duelCli.info(`Start get players stage`);

    let players: Player[] = [];

    try {
        const playerAPromise: Promise<Player> = Player.getPlayerInstanceById(playerAId);
        const playerBPromise: Promise<Player> = Player.getPlayerInstanceById(playerBId);

        players = players.concat(await Promise.all([playerAPromise, playerBPromise]));
    } catch (error) {
        Logger.duelCli.error(error, {
            errorDescription: `Reading player information in database failed`,
            suspectedFunction: Player.getPlayerInstanceById,
            rawData: {playerAId, playerBId}
        });
        process.exit(10);
    }

    Logger.duelCli.info(`End get players stage`);

    return {
        playerA: players[0],
        playerB: players[1]
    };
}
