import RushApi from '../../common/lib/rush-api';
import Logger from '../../common/lib/logger';

export default async function registerDuelResult(duelResultJson: string) {
    Logger.duelCli.info('Start register duel result stage');
    try {
        await RushApi.addDuelResult(duelResultJson);
    } catch (error) {
        const message = 'Duel result can\'t be register';
        Logger.duelCli.error(error, {
            errorDescription: message,
            rawData: duelResultJson
        });
        throw Error(message);
    }
    Logger.duelCli.info('End register duel result stage');
}
