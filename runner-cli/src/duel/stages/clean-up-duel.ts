import Player from '../data-structures/player';
import * as Dockerode from 'dockerode';
import Docker from '../../common/lib/docker';
import {ContainerWrappingScriptData} from './generate-container-wrapping-scripts';
import Logger from '../../common/lib/logger';
import {promises as fsPromise} from 'fs';

type PlayerData = Player & { dockerImageInfo: Dockerode.ImageInspectInfo, script: ContainerWrappingScriptData }

async function cleanUpPlayerScript(player: PlayerData): Promise<void> {
    return fsPromise.unlink(player.script.path);
}

async function cleanUpPlayerDocker(player: PlayerData): Promise<void> {

    /**
     * Clean up player container
     */

    try {
        const container = await Docker.getContainer(player.script.containerName);
        await container.remove({force: true});
        Logger.duelCli.verbose(`Container ${player.script.containerName} was removed`);
    } catch (error) {
        const message = `Container ${player.script.containerName} can't be remove`;
        Logger.duelCli.error(error, {
            errorDescription: message,
            suspectedFunction: Docker.getContainer
        });
    } // Error not fatal

    /**
     * Clean up all player images found (=all tags)
     */
    const removeImagesPromises: Promise<void>[] = player.dockerImageInfo.RepoTags.map(async (imageTag) => {
        try {
            const image: Dockerode.Image = await Docker.getImage(imageTag);
            await image.remove({force: true});
            Logger.duelCli.verbose(`Docker image ${imageTag} was removed`);
        } catch (error) {
            const message = `Docker image ${imageTag} can't be removed`;
            Logger.duelCli.error(error, {
                errorDescription: message,
                suspectedFunction: Docker.getImage
            });
        } // Error not fatal
    });

    await Promise.all(removeImagesPromises);
}

export default async function cleanUpDuelPlayers(playerA: PlayerData, playerB: PlayerData) {
    Logger.duelCli.info('Start clean up duel players stage');
    const players: PlayerData[] = [playerA, playerB];

    const dockerPromises: Promise<void>[] = players.map(cleanUpPlayerDocker);

    const scriptPromises: Promise<void>[] = players.map(cleanUpPlayerScript);

    await Promise.all(dockerPromises.concat(scriptPromises));

    Logger.duelCli.info('End clean up duel players stage');
}
