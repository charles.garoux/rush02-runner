import Player from '../data-structures/player';
import Docker from '../../common/lib/docker';
import Format from '../config/format';
import Logger from '../../common/lib/logger';
import * as Dockerode from 'dockerode';
import {FileSystemPath} from '../../common/data-structures/commons.types';
import {PlayerDefaultGitUriType} from '../../common/data-structures/player.interface';
import * as fs from 'fs';

type StageResult = Promise<{ playerAImageInfo: Dockerode.ImageInspectInfo, playerBImageInfo: Dockerode.ImageInspectInfo }>;

export default async function buildPlayerProgram(playerA: Player, playerB: Player): StageResult {
    Logger.duelCli.info(`Start build stage`);

    const players: Player<PlayerDefaultGitUriType, FileSystemPath>[] = [playerA, playerB]
        .map((player): Player<PlayerDefaultGitUriType, FileSystemPath> => { // Check if player as local repo
            if (player.localRepo === undefined || player.localRepo === null) {
                Logger.duelCli.error(`Player ${player.id} don't have local repo`);
                process.exit(21);
            } else if (!fs.existsSync(player.localRepo)) {
                Logger.duelCli.error(`Player ${player.id} local repo "${player.localRepo}" not found`);
                process.exit(22);
            } else
                return player as Player<PlayerDefaultGitUriType, FileSystemPath>;
        });

    /**
     * Build Docker image
     */

    const playersBuilds = players.map(async (player) => {
        Logger.duelCli.verbose(`Start build player ${player.id}`);
        try {
            const imageName = Format.playerDockerImageFormat(player.id);

            const imageInfo = await Docker.build(`${player.localRepo}/Dockerfile`, imageName);
            Logger.duelCli.verbose(`Finish build Player ${player.id}`);
            if (imageInfo.Config.User === 'root')
                Logger.duelRule.warn(`Docker image "${imageName}" user is root`);
            if (imageInfo.Config.Volumes !== null)
                Logger.duelRule.warn(`Docker image "${imageName}" has volumes`);
            return imageInfo;
        } catch (error) {
            Logger.duelCli.error(error, {
                errorDescription: `Fail during building player ${player.id} Docker image`,
                suspectedFunction: Docker.build
            });
            process.exit(20);
        }
    });

    const images = await Promise.all(playersBuilds);

    Logger.duelCli.info(`End build stage`);
    return {
        playerAImageInfo: images[0],
        playerBImageInfo: images[1]
    };
}
