import {PlayerDefaultGitUriType, PlayerInterface} from '../../common/data-structures/player.interface';
import RushApi from '../../common/lib/rush-api';


export default class Player<GitUriType = PlayerDefaultGitUriType, LocalRepoType = PlayerDefaultGitUriType> implements PlayerInterface<GitUriType, LocalRepoType> {
    id: string;
    gitHttpsUri: GitUriType;
    localRepo: LocalRepoType;

    constructor(id: string, gitHttpsUri: GitUriType, localRepo: LocalRepoType) {
        this.id = id;
        this.gitHttpsUri = gitHttpsUri;
        this.localRepo = localRepo;
    }

    static clone(player: PlayerInterface): Player {
        return new Player(player.id, player.gitHttpsUri, player.localRepo);
    }

    static async getPlayerById(playerId: string): Promise<PlayerInterface> {
        const playerData = await RushApi.getPlayerById(playerId);

        return {
            id: playerData.team_id.toString(),
            gitHttpsUri: playerData.repo_link,
            localRepo: playerData.repo_path
        };
    }

    static async getPlayerInstanceById(playerId: string): Promise<Player> {
        return this.clone(await this.getPlayerById(playerId));
    }
}
