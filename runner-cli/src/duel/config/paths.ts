import {FileSystemPath} from '../../common/data-structures/commons.types';
import {createAllDir} from '../../common/lib/utils';

export default class Paths {
    static artifactDirectory: FileSystemPath;

    static init(artifactDirectory: FileSystemPath) {
        createAllDir([artifactDirectory]);
        this.artifactDirectory = artifactDirectory;
    }

    // static toPlayerRepo(playerId: string): FileSystemPath {
    //     return `${this.artifactDirectory}/tmp/players-repo/${playerId}`;
    // }

    // static toPlayerDockerfile(playerId: string): FileSystemPath {
    //     return `${this.toPlayerRepo(playerId)}/Dockefile`;
    // }

    // static toDuelServerRepo(): FileSystemPath {
    //     return `${this.artifactDirectory}/tmp/duel-server-repo`;
    // }

    // static toDuelServerDockerfile(): FileSystemPath {
    //     return `${this.toDuelServerRepo()}/Dockefile`;
    // }

    static toWrappingScript(playerId: string, extensionName: string = 'bash') {
        return `${this.artifactDirectory}/tmp/wrapping-scripts/${playerId}.${extensionName}`;
    }
}
