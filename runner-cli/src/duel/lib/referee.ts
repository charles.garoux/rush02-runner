import {FileSystemPath} from '../../common/data-structures/commons.types';
import Logger from '../../common/lib/logger';
import * as shell from 'shelljs';
import {execIn} from '../../common/lib/shell-utils';

export type DuelResultType = any

type ExecResult = shell.ExecOutputReturnValue & { command: string, path: FileSystemPath }

export default class Referee {
    static pathToReferee: FileSystemPath;
    static baseCommand = 'cargo run -p referee --';

    static async init(pathToReferee: FileSystemPath) {
        Logger.duelServer.verbose('Init referee', {module: Referee});

        this.pathToReferee = pathToReferee;

        if (!await this._checkIntegrity()) {
            const message = `Referee fail integrity check`;
            Logger.duelServer.error(message, {module: Referee});
            process.exit(31);
        }
    }

    static async _checkIntegrity(): Promise<boolean> {
        Logger.duelServer.verbose('Start check referee', {module: Referee});

        const checkResult = await this._exec('--help');

        if (checkResult.code !== 0) {
            const message = `"${checkResult.command}" exit code "${checkResult.code}" in "${this.pathToReferee}"`;
            Logger.duelServer.error(checkResult.stderr, {
                message: message,
                module: Referee,
                rawData: checkResult
            });
            return false;
        }
        return true;
    }

    static async _exec(cliInput: string, silent: boolean = true): Promise<ExecResult> {
        const command = `${this.baseCommand} ${cliInput}`;
        Logger.duelServer.debug(`Referee CLI command : ${command}`, {module: Referee});

        // @ts-ignore (because ".command" and ".path" is not yet defined but it will be)
        const result: ExecResult = execIn(this.pathToReferee, () => shell.exec(command, {silent: silent}));
        result.command = command;
        result.path = this.pathToReferee;
        return result;
    }

    /**
     *
     * @param playerAProgram
     * @param playerBProgram
     * @return string = duel result in JSON
     */
    static async run(playerAProgram: FileSystemPath, playerBProgram: FileSystemPath): Promise<string> {
        Logger.duelServer.verbose('Start run referee', {module: Referee});
        const flags = '--json';

        const duelExec = await this._exec(`${flags} ${playerAProgram} ${playerBProgram}`);

        if (duelExec.code !== 0) {
            const message = `"${duelExec.command}" exit code "${duelExec.code}" in "${duelExec.path}"`;
            Logger.duelServer.error(duelExec.stderr, {
                message: message,
                module: Referee
            });
            throw Error(message);
        }

        let dataJson: DuelResultType;
        try {
            dataJson = JSON.parse(duelExec.stdout) as DuelResultType;
            Logger.duelServer.debug(`Duel JSON parsed result :`, {module: Referee, rawData: dataJson});
        } catch (error) {
            const message = `Can't parse JSON from referee stdout`;
            Logger.duelServer.error(error, {
                message: message,
                module: Referee,
                rawData: duelExec
            });
            throw Error(message);
        }

        return JSON.stringify(dataJson);
    }
}
