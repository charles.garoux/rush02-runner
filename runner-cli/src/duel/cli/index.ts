import {Command} from '@oclif/command';
import duelFlags from './flags';
import duelArgs from './args';
import Paths from '../config/paths';
import Stage from '../stages';
import Logger from '../../common/lib/logger';
import RushApi from '../../common/lib/rush-api';

export default class Duel extends Command {
    static description = 'Build the players programs then launch the referee by providing the players programs';

    static examples = [
        `$ runner duel --playerA "101" --playerB "42" --apiUsername "arbiter" --apiPassword "qwerty"`
    ];

    static flags = duelFlags;

    static args = duelArgs;

    async run() {
        const {args, flags} = this.parse(Duel);

        /**
         * Init
         */

        Paths.init(flags.artifactDirectory);
        await RushApi.init(flags.apiBaseUrl, flags.apiUsername, flags.apiPassword);

        /**
         * Run CLI
         */

        Logger.duelCli.debug(`Duel : Player ${flags.playerA} vs Player ${flags.playerB}`, {
            rawData: {flags, args}
        });

        const {playerA, playerB} = await Stage.getPlayers(flags.playerA, flags.playerB);

        const {playerAImageInfo, playerBImageInfo} = await Stage.buildPlayerProgram(playerA, playerB);

        const {playerAScript, playerBScript} = await Stage.generateContainerWrappingScripts(
            {...playerA, dockerImageInfo: playerAImageInfo},
            {...playerB, dockerImageInfo: playerBImageInfo});

        await Stage.installDuelServer(flags.refereeDirectory);

        let duelResultJson: string;
        try {
            duelResultJson = await Stage.runDuelServer(playerAScript.path, playerBScript.path);
        } catch (error) {
            Logger.duelCli.error(error, {module: Duel, errorDescription: 'Error during run duel stage'});
            await Stage.cleanUpDuelPlayers({...playerA, dockerImageInfo: playerAImageInfo, script: playerAScript},
                {...playerB, dockerImageInfo: playerBImageInfo, script: playerBScript});
            process.exit(32);
        }

        try {
            await Stage.registerDuelResult(duelResultJson);
        } catch (error) {
            Logger.duelCli.error(error, {module: Duel, errorDescription: 'Error during register duel result stage'});
            await Stage.cleanUpDuelPlayers({...playerA, dockerImageInfo: playerAImageInfo, script: playerAScript},
                {...playerB, dockerImageInfo: playerBImageInfo, script: playerBScript});
            process.exit(50);
        }

        await Stage.cleanUpDuelPlayers({...playerA, dockerImageInfo: playerAImageInfo, script: playerAScript},
            {...playerB, dockerImageInfo: playerBImageInfo, script: playerBScript});
    }

    async catch(error: any) {
        if (error?.oclif?.exit === 0)
            process.exit(0);

        Logger.duelCli.error(error, {
            errorDescription: 'Unhandled error catch',
            module: Duel
        });
        process.exit(255);

        // do something or
        // re-throw to be handled globally
        // throw error;
    }
}
