import {flags} from '@oclif/command';
import {EOL} from 'os';
import {apiAccessFlags} from '../../common/cli/flags';

const duelFlags = {
    help: flags.help({char: 'h'}),
    /**
     * Duel input
     */
    playerA: flags.string({
        char: 'A',
        env: 'PLAYER_A',
        description: 'Player A Id in DB.',
        required: true
    }),
    playerB: flags.string({
        char: 'B',
        env: 'PLAYER_B',
        description: 'Player B Id in DB.',
        required: true
    }),
    /**
     * Config input
     */
    ...apiAccessFlags,
    artifactDirectory: flags.string({
        char: 'a',
        env: 'ARTIFACT_DIRECTORY',
        description: `Directory where CLI artifacts will be stored.${EOL}` +
            '{Right Warning} : Read, write and execute rights are required in the directory.',
        required: false,
        default: '../runner-cli-duel-artifacts'
    }),
    refereeDirectory: flags.string({
        char: 'r',
        env: 'REFEREE_DIRECTORY',
        description: 'Directory where is referee program',
        required: false,
        default: '../../server'
    })
};

export default duelFlags;
