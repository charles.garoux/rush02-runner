import {Command} from '@oclif/command';
import checkFlags from './flags';
import checkArgs from './args';
import Logger from '../../common/lib/logger';
import Stage from '../stages';
import {PlayerTableElement} from '../../common/data-structures/db-tables';
import RushApi from '../../common/lib/rush-api';
import checkAliases from './aliases';
import * as bluebird from 'bluebird';
import Paths from '../config/paths';
import * as fs from 'fs';
import {deleteAllDir} from '../../common/lib/utils';

type PlayerCheckType = { playerData: PlayerTableElement, checkErrorMessage?: string, isValid: boolean }

export default class Check extends Command {
    static description = 'Check players buildable.';

    static aliases = checkAliases;

    static flags = checkFlags;

    static args = checkArgs;

    async run() {
        const {args, flags} = this.parse(Check);

        /**
         * Init
         */

        Paths.init(flags.artifactDirectory);
        await RushApi.init(flags.apiBaseUrl, flags.apiUsername, flags.apiPassword);

        /**
         * Run CLI
         */

        const playersData: PlayerTableElement[] = await Stage.getPlayers(flags.playerIds);

        /**
         * return string = fail check reason
         * return undefined = check OK
         */
        const playerCheckPromises = bluebird.map(playersData, async (playerData: PlayerTableElement): Promise<PlayerCheckType> => {
            Logger.checkCli.info(`Start check player "${playerData.name}"`);

            if (!playerData.repo_link)
                return {playerData, isValid: false, checkErrorMessage: 'Git link not found'};
            const playerDataWithRepoLink = playerData as PlayerTableElement<string>;

            const pathToRepo = await Stage.clonePlayerRepo(playerDataWithRepoLink);

            if (!pathToRepo)
                return {playerData, isValid: false, checkErrorMessage: `Can't clone ${playerData.repo_link}`};


            const pathToDockerfile = `${pathToRepo}/Dockerfile`;
            if (!fs.existsSync(pathToDockerfile)) // Stage : check Dockerfile
                return {playerData, isValid: false, checkErrorMessage: `Can't find Dockerfile in ${pathToDockerfile}`};

            const dockerImage = await Stage.buildPlayerDockerImage(playerData, pathToDockerfile);
            if (!dockerImage)
                return {playerData, isValid: false, checkErrorMessage: `Can't build Docker image`};

            await Stage.deletePlayerDockerImage(dockerImage);

            deleteAllDir([pathToRepo]); // Stage : delete repository

            Logger.checkCli.info(`End check player "${playerData.name}"`);
            return {playerData, isValid: true};
        }, {concurrency: flags.checkConcurrency});

        const checkResult = await bluebird.all(playerCheckPromises);

        await Stage.dockerPruneImages();

        Logger.checkCli.info('Check finish', {
            rawData: checkResult
        });
    }

    async catch(error: any) {
        if (error?.oclif?.exit === 0)
            process.exit(0);

        Logger.checkCli.error(error, {
            errorDescription: 'Unhandled error catch',
            module: Check
        });
        process.exit(255);

        // do something or
        // re-throw to be handled globally
        // throw error;
    }
}
