import {flags} from '@oclif/command';
import {EOL} from 'os';
import {apiAccessFlags} from '../../common/cli/flags';

const checkFlags = {
    help: flags.help({char: 'h'}),
    /**
     * Check input
     */
    playerIds: flags.string({
        char: 'p',
        description: 'Player id list to check, if empty : all players will be check.',
        required: false,
        multiple: true,
        default: []
    }),
    checkConcurrency: flags.integer({
        char: 'c',
        env: 'CHECK_CONCURRENCY',
        description: 'Number of players to check at the same time.',
        required: false,
        default: 1
    }),
    /**
     * Config input
     */
    ...apiAccessFlags,
    artifactDirectory: flags.string({
        char: 'a',
        env: 'ARTIFACT_DIRECTORY',
        description: `Directory where CLI artifacts will be stored.${EOL}` +
            '{Right Warning} : Read and write rights are required in the directory.',
        required: false,
        default: '../runner-cli-check-artifacts'
    })
};

export default checkFlags;
