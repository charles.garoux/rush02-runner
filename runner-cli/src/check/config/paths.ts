import {FileSystemPath} from '../../common/data-structures/commons.types';
import {createAllDir} from '../../common/lib/utils';

export default class Paths {
    static artifactDirectory: FileSystemPath;

    static init(artifactDirectory: FileSystemPath) {
        createAllDir([artifactDirectory]);
        this.artifactDirectory = artifactDirectory;
    }

    static toPlayerRepo(playerId: string): FileSystemPath {
        return `${this.artifactDirectory}/tmp/players-repo/${playerId}`;
    }
}
