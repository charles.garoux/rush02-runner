export default class Format {
    static playerDockerImageFormat(playerId: string): string {
        return `player-${playerId}-ai`;
    }
}
