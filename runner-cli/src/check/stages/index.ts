import getPlayers from './get-players';
import clonePlayerRepo from './clone-player-repo';
import buildPlayerDockerImage from './build-player-docker-image';
import deletePlayerDockerImage from './delete-player-docker-image';
import dockerPruneImages from './docker-prune-image';

export default {
    getPlayers,
    clonePlayerRepo,
    buildPlayerDockerImage,
    deletePlayerDockerImage,
    dockerPruneImages
};
