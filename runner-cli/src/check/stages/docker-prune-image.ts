import Docker from '../../common/lib/docker';
import Logger from '../../common/lib/logger';

export default async function dockerPruneImages() {
    Logger.checkCli.info('Start Docker images prune stage');
    try {
        await Docker.pruneImages();
        Logger.checkCli.verbose('Docker images prune was succeed');
    } catch (error) {
        const message = 'Can\'t prune Docker images';
        Logger.checkCli.error(error, {
            errorDescription: message,
            suspectedFunction: Docker.pruneImages
        });
    }
    Logger.checkCli.info('End Docker images prune stage');
}
