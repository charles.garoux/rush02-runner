import * as Dockerode from 'dockerode';
import Docker from '../../common/lib/docker';
import Logger from '../../common/lib/logger';

export default async function deletePlayerDockerImage(dockerImageInfo: Dockerode.ImageInspectInfo) {

    const removeImagesPromises: Promise<void>[] = dockerImageInfo.RepoTags.map(async (imageTag) => {
        try {
            const image: Dockerode.Image = await Docker.getImage(imageTag);
            await image.remove({force: true});
            Logger.duelCli.verbose(`Docker image ${imageTag} was removed`);
        } catch (error) {
            const message = `Docker image ${imageTag} can't be removed`;
            Logger.duelCli.error(error, {
                errorDescription: message,
                suspectedFunction: Docker.getImage
            });
        } // Error not fatal
    });

    await Promise.all(removeImagesPromises);
}
