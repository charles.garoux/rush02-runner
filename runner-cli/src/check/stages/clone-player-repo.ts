import Logger from '../../common/lib/logger';
import {PlayerTableElement} from '../../common/data-structures/db-tables';
import {FileSystemPath} from '../../common/data-structures/commons.types';
import Paths from '../config/paths';
import Git from '../../common/lib/git';
import {resetAllDir} from '../../common/lib/utils';

/**
 *
 * @param player
 * @return path to repo
 */
export default async function clonePlayerRepo(player: PlayerTableElement<string>): Promise<FileSystemPath | undefined> {
    Logger.checkCli.info(`Start clone player "${player.team_id}" repo stage`);

    let pathToRepo: FileSystemPath | undefined;
    try {
        const localRepo = Paths.toPlayerRepo(player.team_id.toString());
        resetAllDir([localRepo]);
        pathToRepo = await Git.cloneProject(player.repo_link, localRepo);
        Logger.checkCli.verbose(`Player "${player.team_id} repository was cloned`);
    } catch (error) {
        const message = `Can't clone player "${player.team_id} repository"`;
        Logger.checkCli.error(error, {
            errorDescription: message,
            suspectedFunction: Git.cloneProject
        });
        Logger.checkRule.error(message);
        pathToRepo = undefined;
    }

    Logger.checkCli.info(`Start clone player "${player.team_id}" repo stage`);

    return pathToRepo;
}
