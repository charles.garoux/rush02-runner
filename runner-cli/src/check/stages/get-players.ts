import Logger from '../../common/lib/logger';
import {PlayerTableElement} from '../../common/data-structures/db-tables';
import RushApi from '../../common/lib/rush-api';
import * as bluebird from 'bluebird';

export default async function getPlayers(playerIds: string[]): Promise<PlayerTableElement[]> {
    Logger.checkCli.info('Start get players stage');

    let players: PlayerTableElement[];
    if (playerIds.length > 0) {
        const playersDataPromises = bluebird.map(playerIds, async (playerId) => {
            let playerData: PlayerTableElement | undefined;

            try {
                playerData = await RushApi.getPlayerById(playerId);
                Logger.checkCli.verbose(`Player ${playerId} found`);
            } catch (error) {
                const message = `Player ${playerId} not found`;
                Logger.checkCli.error(error, {
                    errorDescription: message,
                    suspectedFunction: RushApi.getPlayerById
                });
                Logger.checkRule.error(message);
                playerData = undefined;
            }
            return playerData;
        });

        players = await bluebird.all(playersDataPromises)
            .filter((player: PlayerTableElement | undefined) => player !== undefined) as PlayerTableElement[];
    } else {
        try {
            players = await RushApi.getAllPlayers();
            Logger.checkCli.verbose('Players found');
        } catch (error) {
            const message = 'Players not found';
            Logger.checkCli.error(error, {
                errorDescription: message,
                suspectedFunction: RushApi.getAllPlayers
            });
            players = [];
        }
    }

    Logger.checkCli.info('End get player stage');

    return players;
}
