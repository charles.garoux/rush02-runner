import {PlayerTableElement} from '../../common/data-structures/db-tables';
import Logger from '../../common/lib/logger';
import Format from '../config/format';
import {FileSystemPath} from '../../common/data-structures/commons.types';
import Docker from '../../common/lib/docker';
import * as Dockerode from 'dockerode';

export default async function buildPlayerDockerImage(playerData: PlayerTableElement, pathToDockerfile: FileSystemPath) {
    Logger.checkCli.info(`Start build player Docker image for ${playerData.team_id}`);
    const dockerImageName = Format.playerDockerImageFormat(playerData.team_id.toString());
    let dockerImage: Dockerode.ImageInspectInfo | undefined;
    try {
        dockerImage = await Docker.build(pathToDockerfile, dockerImageName);
        Logger.checkCli.verbose(`Player ${playerData.team_id} Docker image was build`);
    } catch (error) {
        const message = `Can't build player ${playerData.team_id} Docker image`;
        Logger.checkCli.error(error, {
            errorDescription: message,
            suspectedFunction: Docker.build
        });
        return undefined;
    }

    Logger.checkCli.info(`End build player Docker image for ${playerData.team_id}`);
    return dockerImage;
}
