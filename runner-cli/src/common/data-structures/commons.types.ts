export type FileSystemPath = string;

export type GitHttpsUri = string;
