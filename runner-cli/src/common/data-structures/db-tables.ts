export interface PlayerTableElement<linkType = string | undefined, pathType = string | undefined> {
    team_id: number
    name: string
    repo_link: linkType
    repo_path: pathType
}
