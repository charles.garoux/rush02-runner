import {FileSystemPath, GitHttpsUri} from './commons.types';

export type PlayerDefaultGitUriType = GitHttpsUri | undefined | null
export type PlayerDefaultLocalRepoType = FileSystemPath | undefined | null

export interface PlayerInterface<GitUriType = PlayerDefaultGitUriType, LocalRepoType = PlayerDefaultLocalRepoType> {
    /**
     * DB
     */
    id: string // Id in DB
    gitHttpsUri: GitUriType

    /**
     * APP
     */
    localRepo: LocalRepoType
}
