export enum WinstonLevel {
    error = 0,
    warn = 1,
    info = 2,
    http = 3,
    verbose = 4,
    debug = 5,
    silly = 6,
}


export type MetadataTemplate = {
    level?: string,
    message?: string,
    element?: string,
    module?: { name: string },
    rawData?: any,
    stack?: any,
    suspectedFunction?: ((...x: any) => any),
    errorDescription?: string
}
