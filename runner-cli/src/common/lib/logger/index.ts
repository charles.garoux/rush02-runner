import * as winston from 'winston';
import {toPrettyJson} from '../json';
import {MetadataTemplate} from './data-structures';
import chalk from 'chalk';
import {EOL} from 'os';

const defaultMinLevel: string = process?.env?.LOG_LEVEL?.toString() as string | 'verbose';

const defaultFormat = winston.format.json();

export const defaultConsoleFormat = winston.format.printf(({level, message, element, module, rawData, stack, controller, suspectedFunction, errorDescription, ...info}: winston.Logform.TransformableInfo & MetadataTemplate) => {
    const elementFormat = (element !== undefined) ? ` [${chalk.magenta(element)}]` : '';
    const moduleFormat = (module !== undefined) ? ` [module: ${module.name}]` : '';
    const rawDataFormat = (rawData !== undefined) ? ` [raw data JSON : ${toPrettyJson(rawData)}]` : '';
    const suspectedFunctionFormat = (suspectedFunction !== undefined) ? ` [suspected function : ${suspectedFunction.name}]` : '';
    const errorDescriptionFormat = (errorDescription !== undefined) ? ` ${errorDescription} :${EOL}` : '';

    return `[${level}]${elementFormat}${moduleFormat}${errorDescriptionFormat} ${stack || message}${suspectedFunctionFormat}${rawDataFormat}`;
});

const defaultTransports = [
    new winston.transports.File({filename: 'error.log', level: 'error'}),
    // new winston.transports.File({filename: 'combined.log'}),
    new winston.transports.Console({
        format: winston.format.combine(
            winston.format.colorize(),
            winston.format.errors({stack: true}),
            defaultConsoleFormat
        )
    })
];

interface WinstonLoggerCustom extends Omit<winston.Logger, 'error'> {
    error: { (error: string | Error, meta?: MetadataTemplate): Logger }
    originalError: winston.LeveledLogMethod
}

function generateDefaultLogger(winstonLoggerBase: WinstonLoggerCustom, appElement: string, metadata?: any) {
    const meta = {
        element: appElement,
        ...metadata
    };

    const logger: WinstonLoggerCustom = Object.create(winstonLoggerBase);

    logger.defaultMeta = meta;

    return logger;
}

function generateCustomLogger(): WinstonLoggerCustom {
    // @ts-ignore (because ".originalError" is not yet defined but it will be)
    let winstonLogger: WinstonLoggerCustom = winston.createLogger({
        level: defaultMinLevel,
        format: defaultFormat,
        transports: defaultTransports
    });

    // Overwrite .error() to handle Error instance to show and store stacktrace
    winstonLogger.originalError = winstonLogger.error as winston.LeveledLogMethod;
    winstonLogger.error = function (error, meta) {
        meta = (typeof meta === 'undefined') ? {} : meta;
        if (error instanceof Error) {
            meta.stack = error.stack;
            return this.originalError(error.message, meta);
        } else
            return this.originalError(error, meta);
    };

    return winstonLogger;
}

export default class Logger {
    static winston: WinstonLoggerCustom = generateCustomLogger();
    /**
     * Duel CLI Command specific log
     */

    static duelCli = generateDefaultLogger(Logger.winston, 'duel cli');
    static duelRule = generateDefaultLogger(Logger.winston, 'duel rule');

    /**
     * Clone CLI Command specific log
     */

    static cloneCli = generateDefaultLogger(Logger.winston, 'clone cli');

    /**
     * Check CLI Command specific log
     */

    static checkCli = generateDefaultLogger(Logger.winston, 'check cli');
    static checkRule = generateDefaultLogger(Logger.winston, 'check rule');

    /**
     * Common
     */

    static clone = generateDefaultLogger(Logger.winston, 'clone');
    static build = generateDefaultLogger(Logger.winston, 'build');

    static git = generateDefaultLogger(Logger.winston, 'git');
    static docker = generateDefaultLogger(Logger.winston, 'docker');
    static duelServer = generateDefaultLogger(Logger.winston, 'referee');
    static api = generateDefaultLogger(Logger.winston, 'api');

    static other = generateDefaultLogger(Logger.winston, 'other');
}
