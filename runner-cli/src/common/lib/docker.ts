import * as Dockerode from 'dockerode';
import {FileSystemPath} from '../data-structures/commons.types';
import * as path from 'path';
import * as shell from 'shelljs';
import {ChildProcess} from 'child_process';
import Logger from './logger';

const docker = new Dockerode();

export default class Docker {

    /**
     * @param pathToDockerfile
     * @param imageName
     * @return docker image name
     */
    static async build(pathToDockerfile: FileSystemPath, imageName: string): Promise<Dockerode.ImageInspectInfo> {
        return new Promise((resolve, reject) => {
            const command = `docker build '${path.dirname(pathToDockerfile)}' -t '${imageName}'`;

            Logger.other.debug(`Shell exec command : ${command}`);
            const buildChild: ChildProcess = shell.exec(command, {
                silent: true,
                async: true
            });

            buildChild.on('exit', (code: number | null, signal: string | null) => {
                const errorMessage = `Build Docker image failed "${imageName}" from Dockerfile "${pathToDockerfile}", shell command : "${command}"`;
                if (code !== 0) {
                    Logger.build.error(`Exit "${code}", signal "${signal}"`, {
                        errorDescription: errorMessage,
                        module: Docker,
                        rawData: {command, code, signal}
                    });
                    reject(new Error(`Docker build exit "${code}" with signal "${signal}"`));
                } else {
                    const image = docker.getImage(imageName);

                    image.inspect()
                        .then((info: Dockerode.ImageInspectInfo) => {
                            Logger.build.verbose(`Successful Build Docker image "${imageName}" from Dockerfile "${pathToDockerfile}"`, {module: Docker});
                            resolve(info);
                        })
                        .catch((reason) => {
                            Logger.build.error(reason, {
                                errorDescription: errorMessage,
                                module: Docker
                            });
                            reject(new Error(reason));
                        });
                }
            });
        });

        // Other solution du build, but error occurred :
        // Error: (HTTP code 500) server error - unexpected EOF
        // Github issue : https://github.com/apocas/docker-modem/issues/137
        // return docker.buildImage(pathToDockerfile, {
        //     t: imageName
        // });
        // OR
        // return docker.buildImage({context: path.dirname(pathToDockerfile), src: ['Dockerfile']}, {
        //     t: imageName
        // });
    }

    static async run(image: string, outputStream: NodeJS.WritableStream | NodeJS.WritableStream[] = process.stdout) {
        return docker.run(image, [], outputStream, []);
    }

    static async getImage(image: string): Promise<Dockerode.Image> {
        return docker.getImage(image);
    }

    static async getContainer(containerName: string): Promise<Dockerode.Container> {
        return docker.getContainer(containerName);
    }

    static async pruneImages(options?: Dockerode.PruneImagesInfo) {
        return docker.pruneImages(options);
    }

}
