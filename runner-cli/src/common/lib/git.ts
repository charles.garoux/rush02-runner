import simpleGit, {SimpleGit, SimpleGitOptions} from 'simple-git';
import {FileSystemPath, GitHttpsUri} from '../data-structures/commons.types';
import Logger from '../lib/logger';

process.env.GIT_TERMINAL_PROMPT = '0'; // Needed to avoid prompt by Git (exemple: when repo is private)

const options: Partial<SimpleGitOptions> = {
    baseDir: process.cwd(),
    binary: 'git',
    maxConcurrentProcesses: 6
};
const git: SimpleGit = simpleGit(options);

export default class Git {

    /**
     * Warning : reject promise on timeout
     *
     * @param gitUri
     * @param destinationDirectory
     * @return path to local repo
     */
    static async cloneProject(gitUri: GitHttpsUri, destinationDirectory: FileSystemPath): Promise<string> {
        return new Promise<string>(async (resolve, reject) => {
            Logger.other.verbose(`Start cloning "${gitUri}" to "${destinationDirectory}"`, {module: Git});
            try {
                await git.clone(gitUri, destinationDirectory);
            } catch (error) {
                const message = `Can't clone ${gitUri} to ${destinationDirectory}`;
                Logger.other.error(error, {
                    suspectedFunction: git.clone,
                    module: Git
                });
                reject(message);
            }
            Logger.other.verbose(`Finish cloning "${gitUri}" to "${destinationDirectory}"`, {module: Git});

            resolve(destinationDirectory);
        });
    }
}
