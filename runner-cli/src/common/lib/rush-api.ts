import axios, {AxiosInstance, AxiosResponse, AxiosError} from 'axios';
import {PlayerTableElement} from '../data-structures/db-tables';
import Logger from './logger';
import * as util from 'util';
import {DuelResultType} from '../../duel/lib/referee';
import {FileSystemPath} from '../data-structures/commons.types';

export default class RushApi {
    static _axiosClient: AxiosInstance;


    static _logQueryError(error: any, errorDescription: string, suspectedFunction?: ((...x: any) => any), rawData?: object) {
        if (axios.isAxiosError(error)) {
            const axiosError = error as AxiosError;
            Logger.api.error(error, {
                errorDescription: errorDescription,
                rawData: {...rawData, axiosError: axiosError.toJSON()},
                suspectedFunction: this._axiosClient.post
            });
        } else {
            Logger.api.error(error, {
                errorDescription: errorDescription,
                suspectedFunction: this._axiosClient.put,
                rawData: {...rawData}
            });
        }
    }

    static _logQueryResultDebug(message: string, response: AxiosResponse) {
        Logger.api.debug(`Players found`, {
            module: RushApi, rawData: {
                data: response?.data,
                header: response?.headers,
                status: response?.status
            }
        });
    }

    /**
     * Init HTTP client to communicate with the API
     * @param baseURL Example : http://localhost:8080
     * @param username
     * @param password
     */
    static async init(baseURL: string, username: string, password: string): Promise<void> {

        try {
            const response = await axios.post<{ token: string }>(`${baseURL}/login`, {username, password}, {baseURL});

            this._axiosClient = axios.create({
                baseURL,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${response.data.token}`
                },
                timeout: 2000
            });
        } catch (error) {
            const message = `Can't create API HTTP client`;
            Logger.api.error(error, {errorDescription: message});
            throw Error(message);
        }
    }

    static async getAllPlayers(): Promise<PlayerTableElement[]> {
        let response;
        try {
            response = await this._axiosClient.get<PlayerTableElement[] | null>('/allTeams');
            this._logQueryResultDebug(`Players found`, response);
            if (!response.data) return [];
        } catch (error) {
            const message = `Players not found`;
            this._logQueryError(error, message, this._axiosClient.post);
            throw Error(message);
        }

        return response.data;
    }

    static async getPlayerById(playerId: number | string): Promise<PlayerTableElement> {
        const id: number = (typeof playerId === 'string') ? parseInt(playerId, 10) : playerId;

        let response: AxiosResponse<PlayerTableElement>;
        try {
            response = await this._axiosClient.get<PlayerTableElement>('/team', {
                data: {team_id: id}
            });
            this._logQueryResultDebug(`Player ${id} found`, response);
        } catch (error) {
            const message = `Player ${id} can't be found`;
            this._logQueryError(error, message, this._axiosClient.get, {playerId});
            throw Error(message);
        }

        return response.data;
    }

    static async setTeamRepoPath(playerId: number | string, pathToRepo: FileSystemPath) {
        const id: number = (typeof playerId === 'string') ? parseInt(playerId, 10) : playerId;

        let response: AxiosResponse<void>;
        try {
            response = await this._axiosClient.post<void>('/auth/setTeamRepoPath', {
                team_id: id,
                repo_path: pathToRepo
            });
            this._logQueryResultDebug(`Player ${playerId} repo_path updated`, response);
        } catch (error) {
            const message = `Player ${playerId} repo_path can't be update`;
            this._logQueryError(error, message, this._axiosClient.post, {playerId, pathToRepo});
            throw Error(message);
        }

        return response.data;
    }

    static async addDuelResult(duelResult: DuelResultType): Promise<any> {
        let response: AxiosResponse<PlayerTableElement[]>;
        try {
            response = await this._axiosClient.post('/auth/updateGame', duelResult);
            Logger.api.debug(`Duel result posted`, {
                module: RushApi,
                rawData: {
                    data: response?.data,
                    header: response?.headers,
                    status: response?.status
                }
            });
        } catch (error) {
            const message = `Duel result can't be posted`;
            this._logQueryError(error, message, this._axiosClient.post, {duelResult});
            throw Error(message);
        }

        return response.data;
    }

}
