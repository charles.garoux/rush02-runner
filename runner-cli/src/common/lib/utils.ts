import * as fs from 'fs';
import {FileSystemPath} from '../data-structures/commons.types';

export function resetAllDir(dirToReset: FileSystemPath[]) {
    deleteAllDir(dirToReset);
    createAllDir(dirToReset);
}

export function deleteAllDir(dirToDelete: FileSystemPath[]) {
    dirToDelete.forEach(dirPath => {
        if (fs.existsSync(dirPath)) {
            // @ts-ignore, "options" param is valid : https://nodejs.org/docs/latest-v14.x/api/fs.html#fs_fs_rmdirsync_path_options
            fs.rmdirSync(dirPath, {recursive: true});
        }
    });
}

export function createAllDir(dirToCreate: FileSystemPath[]) {
    dirToCreate.forEach(dirPath => {
        if (!fs.existsSync(dirPath))
            fs.mkdirSync(dirPath, {recursive: true});
    });
}
