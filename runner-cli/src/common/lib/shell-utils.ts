import {FileSystemPath} from '../data-structures/commons.types';
import * as shell from 'shelljs';

/**
 * Example : execIn('/tmp', () => shell.exec('ls'));
 * @param path
 * @param callback
 */

export function execIn(path: FileSystemPath, callback: () => shell.ShellString): shell.ShellString {
    const orignalPosition = shell.pwd().toString();

    shell.cd(path);

    const result = callback();
    shell.cd(orignalPosition);
    return result;
}
