import {flags} from '@oclif/command';

export const apiAccessFlags = {
    apiUsername: flags.string({
        env: 'API_USERNAME',
        description: 'Username for API.',
        required: true
    }),
    apiPassword: flags.string({
        env: 'API_PASSWORD',
        description: 'Password for API.',
        required: true
    }),
    apiBaseUrl: flags.string({
        env: 'API_BASE_URL',
        description: 'Base URL to access the API.',
        required: false,
        default: 'http://localhost:3000'
    })
};
