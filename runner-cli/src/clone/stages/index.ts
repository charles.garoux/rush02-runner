import getAllPlayersWithRepo from './get-all-players-with-repo';
import clonePlayersRepo from './clone-players-repo';
import updatePlayers from './update-players';

export default {
    getAllPlayersWithRepo,
    clonePlayersRepo,
    updatePlayers
};
