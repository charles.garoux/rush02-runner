import {PlayerTableElement} from '../../common/data-structures/db-tables';
import * as bluebird from 'bluebird';
import Logger from '../../common/lib/logger';
import RushApi from '../../common/lib/rush-api';

export default async function updatePlayers(players: PlayerTableElement<string, string>[], concurrency: number = 1): Promise<void> {
    Logger.cloneCli.info('Start updating player stage');

    await bluebird.map(players, async (player) => {
        await RushApi.setTeamRepoPath(player.team_id, player.repo_path);
    }, {concurrency: concurrency});
}
