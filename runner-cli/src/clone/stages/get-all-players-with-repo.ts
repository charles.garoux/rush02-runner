import Logger from '../../common/lib/logger';
import {PlayerTableElement} from '../../common/data-structures/db-tables';
import RushApi from '../../common/lib/rush-api';

export default async function getAllPlayersWithRepo(): Promise<PlayerTableElement<string>[]> {
    Logger.cloneCli.info('Start getting all players with repositories stage');
    const players = await RushApi.getAllPlayers();

    return players
        .filter(player => {
            if (player.repo_link === undefined) {
                Logger.cloneCli.warn(`Player ${player.name} don't have repo link`);
                return false;
            }
            return true;
        }) as PlayerTableElement<string>[];
}
