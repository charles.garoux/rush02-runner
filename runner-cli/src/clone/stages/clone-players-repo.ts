import {PlayerTableElement} from '../../common/data-structures/db-tables';
import {FileSystemPath} from '../../common/data-structures/commons.types';
import * as bluebird from 'bluebird';
import Git from '../../common/lib/git';
import * as fs from 'fs';
import Logger from '../../common/lib/logger';
import * as path from 'path';

export default async function clonePlayersRepo(players: PlayerTableElement<string>[], targetDirectory: FileSystemPath, cloningConcurrency: number = 1): Promise<PlayerTableElement<string, string>[]> {
    Logger.cloneCli.info(`Start cloning players repositories stage`);

    const clonePromises: Promise<(PlayerTableElement<string, string> | undefined)[]> = bluebird.map(players, async (player): Promise<PlayerTableElement<string, string> | undefined> => {
        const pathToRepo = path.resolve(`${targetDirectory}/${player.name}`);
        if (!fs.existsSync(pathToRepo)) {
            try {
                player.repo_path = await Git.cloneProject(player.repo_link, pathToRepo);
            } catch (error) {
                const message = `Can't clone ${player.name} repository`;
                Logger.cloneCli.error(error, {
                    errorDescription: message,
                    suspectedFunction: Git.cloneProject
                });
                return undefined;
            }
        } else {
            player.repo_path = pathToRepo;
            Logger.cloneCli.warn(`Player ${player.name} local repo already exist in "${pathToRepo}"`);
        }

        return player as PlayerTableElement<string, string>;
    }, {concurrency: cloningConcurrency});

    return bluebird.all(clonePromises)
        .then((players: (PlayerTableElement<string, string> | undefined)[]): PlayerTableElement<string, string>[] => {
            return players.filter(player => player !== undefined) as PlayerTableElement<string, string>[];
        }) as Promise<PlayerTableElement<string, string>[]>;
}
