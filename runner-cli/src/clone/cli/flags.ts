import {flags} from '@oclif/command';
import {apiAccessFlags} from '../../common/cli/flags';

const cloneFlags = {
    help: flags.help({char: 'h'}),
    /**
     * Clone input
     */
    targetDirectory: flags.string({
        char: 't',
        env: 'TARGET_DIRECTORY',
        description: 'Directory where all repositories will be clone.',
        default: '../runner-cli-clone-artifacts'
    }),
    cloneConcurrency: flags.integer({
        char: 'c',
        env: 'CLONE_CONCURRENCY',
        description: 'Git cloning concurrency (= number of simultaneous git clone).',
        default: 1
    }),
    updatePlayersConcurrency: flags.integer({
        char: 'C',
        env: 'UPDATE_PLAYERS_CONCURRENCY',
        description: 'Players updating DB concurrency (= number of simultaneous player update in DB).',
        default: 1
    }),
    /**
     * Config input
     */
    ...apiAccessFlags
};

export default cloneFlags;
