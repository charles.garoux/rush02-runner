import {Command} from '@oclif/command';
import cloneFlags from './flags';
import cloneArgs from './args';
import Logger from '../../common/lib/logger';
import Stage from '../stages';
import {PlayerTableElement} from '../../common/data-structures/db-tables';
import RushApi from '../../common/lib/rush-api';
import cloneAliases from './aliases';

export default class Clone extends Command {
    static description = 'Clones all players repositories.';

    static aliases = cloneAliases;

    static flags = cloneFlags;

    static args = cloneArgs;

    async run() {
        const {args, flags} = this.parse(Clone);

        /**
         * Init
         */

        await RushApi.init(flags.apiBaseUrl, flags.apiUsername, flags.apiPassword);

        /**
         * Run CLI
         */

        const playersWithRepo: PlayerTableElement<string>[] = await Stage.getAllPlayersWithRepo();

        const playersWithLocalRepo: PlayerTableElement<string, string>[] = await Stage.clonePlayersRepo(playersWithRepo, flags.targetDirectory, flags.cloneConcurrency);

        await Stage.updatePlayers(playersWithLocalRepo, flags.updatePlayersConcurrency);

        Logger.cloneCli.info('Clone finish');
    }

    async catch(error: any) {
        if (error?.oclif?.exit === 0)
            process.exit(0);

        Logger.cloneCli.error(error, {
            errorDescription: 'Unhandled error catch',
            module: Clone
        });
        process.exit(255);

        // do something or
        // re-throw to be handled globally
        // throw error;
    }
}
