# Démonstration

**Outil d'enregistrement** : https://asciinema.org

## Commande `check`
[![asciicast](https://asciinema.org/a/BmQ7JWyv3CYjfVZnbb5dScFBv.svg)](https://asciinema.org/a/BmQ7JWyv3CYjfVZnbb5dScFBv)

## Commande `clone`
[![asciicast](https://asciinema.org/a/gi6x0QTD4Wf3LCf8uDGkOUsBU.svg)](https://asciinema.org/a/gi6x0QTD4Wf3LCf8uDGkOUsBU)

## Commande `duel`
[![asciicast](https://asciinema.org/a/zQEpxz8mQoK1DKtlABqWbFhEE.svg)](https://asciinema.org/a/zQEpxz8mQoK1DKtlABqWbFhEE)

