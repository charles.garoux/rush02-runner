# Runner CLI

**Présentation** :
    Outil en ligne de commande (CLI) pour gérer différentes étapes du Rush02.

**Installation** :
```bash
npm install
chmod +x ./bin/run
```

**Usage de base** :
```shell script
./bin/run -h
```

## Les sous-commandes
> Voir les **usages** avec le flag `-h`

* `check` : vérifier les joueurs en capacité d'être build par le runner
* `clone` : clone les dépôts de tous les joueurs
* `duel` : build les programmes des joueurs puis lance l’arbitre en lui fournissant les programmes des joueurs

## Variables d’environnements

* **LOG_LEVEL** = Niveau d'affichage des logs
(Possibilités dans l'ordre d'importance : `error`,`warn`,`info`,`http`,`verbose`,`debug` ou `silly`)
(Voir les niveaux de log de **Winston**: https://www.npmjs.com/package/winston#logging-levels)

## Environnement
   * Backend du rush (API & BDD) : https://gitlab.com/mhouppin/rush02

## Autre

**Stack technique** :
* Node.js 14.x (LTS)
* Typescript
* Oclif
* Docker

**Documentation** : [doc/readme.md](doc/readme.md)

**Auteurs** :
* **Charles Garoux**, chgaroux (développeur)
* **Guillian Hernandez**, guhernan (chef de projet)
