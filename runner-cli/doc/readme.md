# Documentation

**Exit codes** : [exit-codes.md](exit-codes.md)

## Schema de fonctionnement de la CLI

**Schema du fonctionnement de la commande `check`** :

![Schema du fonctionnement de la commande check](schemas/check-command.png "Schema du fonctionnement de la commande 'check'")

**Schema du fonctionnement des commandes `clone` et `duel`** :

![Schema du fonctionnement des commandes 'check' et 'duel'](schemas/clone-and-duel-commands.png "Schema du fonctionnement des commandes 'check' et 'duel'")

## Les dossiers

### drawio-schemas-src/
**Description** :
    Contient les sources draw.io des schémas de la CLI.

### for-dev/
**Description** :
    Contient la documentation pour les développeurs qui auront à travailler sur la CLI.

### schemas/
**Description** :
    Contient les images des schémas de la CLI.

## Script
### export-drawio-schema.bash
**Description** :
    Script pouvant générer des exports en images des sources et supprimer les exports non souhaités.

**Problème** : les images générées ne sont pas exactement celles visibles dans l'éditeur draw.io. Certains éléments ont des tailles différentes ce qui peut poser des problèmes de visualisation.
