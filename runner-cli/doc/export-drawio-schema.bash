#!/usr/bin/env bash

# Dependencies : Docker, sudo

# Right : sudo and current "rwx" on current directory (recursive)

# Description : Export schema from draw.io sources and remove not wanted files

EXPORTER_EXPORT_DIR="../schemas"
LOCAL_EXPORT_DIR="./schemas"

sudo -v

# Run exportation
docker run --rm -it -v "$(pwd)":/data rlespinasse/drawio-export \
  --output "$EXPORTER_EXPORT_DIR" \
  --format png

# Set owner (reason : files create with root in Docker container)
sudo chown "$(id -u):$(id -g)" -R "$LOCAL_EXPORT_DIR"

# Remove not wanted export
find $LOCAL_EXPORT_DIR \( \
  -iname '*not-implemented*' -o \
  -iname '*non-implémenté*' -o \
  -iname '*no-export*' \
  \) \
  -type f \
  -exec echo "Remove" {} \; \
  -exec rm -f {} +
