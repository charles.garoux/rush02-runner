# CLI Architecture

## Points d'entrée des commandes

* `check` : 
    * Fichier : `src/check/cli/index.ts`
    * Fonction : `run()`
* `clone` :
    * Fichier : `src/clone/cli/index.ts`
    * Fonction : `run()`
* `duel` :
    * Fichier : `src/duel/cli/index.ts`
    * Fonction : `run()`


## src/common/*
**Description** :
	Sources en commun des commandes de la CLI.

## src/common/lib/rush-api.ts
**Description** :
    Source du client HTTP pour l’API.

## src/commands/(`check`|`clone`|`duel`)
**Description** : Dossier propre à l’architecture d'Oclif.
    Chaque fichier au nom de sous commandes (`check`|`clone`|`duel`) sert à Oclif de point d'entrée pour chaque sous-commandes.

## src/(`check`|`clone`|`duel`)/*
**Description** :
	Sources de la commande concernée (`check`|`clone`|`duel`) de la CLI.

## src/(`check`|`clone`|`duel`)/stages/*
**Description** :
	Sources des étapes d'exécution de la commande concernée (`check`|`clone`|`duel`) de la CLI.
