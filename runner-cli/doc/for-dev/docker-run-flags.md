
## Docker run Flags

### Run command

All in one :
`docker run --cpus="1" --memory="128m" --kernel-memory="192m" --memory-swap="192m" --network none`

### List of used flags

`--cpus=<value>`
- Limit the specific CPUs or cores a container can use.
- here : 1 core
- so : `"1"`

`-m | --memory`
- The maximum amount of memory the container can use.
- here : 128mo

`--kernel-memory`
- For the kernel memory.
- Why ? Because this is not swappable, so it can block host processes
- More than --memory : "If the kernel memory limit is higher than the user memory limit, the kernel limit does not cause the container to experience an OOM."

`--memory-swap`
- Amount of memory that can be swapp on disk
- If == to --memory  = no swapp is allowed
- default = 128mo
- probably 192mo

`--network`
- `none` to disable networking (all types)  : `--network none`
- Other possibilities -> `brige` `host` `overlay` `macvlan`.

