# Exit code

**Présentation** :
    Liste des codes d'arrêts (exit) du processus défini de la CLI.
    
**Code libre** :
* 8-63,
* 79-125,
* 166-199
* 243-255
> Source : https://unix.stackexchange.com/questions/604260/best-range-for-custom-exit-code-in-linux

**Code** :
* Duel :
    * 10 : erreur lors de la récupération d'un joueur en BDD
    * 20 : erreur lors du build de l’image Docker de joueur
    * 21 : un joueur n'a pas le chemin vers le dépôt Git local
    * 22 : un joueur a un chemin vers le dépôt Git local qui n'existe pas
    * 30 : erreur lors du build de l’image Docker de l'arbitre
    * 31 : erreur lors de la verification de l'integrité de l'arbitre
    * 32 : erreur lors de la phase d'exécution du duel
    * 40 : erreur lors de la generation du script de wrapping d'un joueur
    * 50 : erreur lors de la phase d’enregistrement du résultat du duel

* Autre :
    * **255** : erreur imprévue par la CLI (à remonter d’urgence)
